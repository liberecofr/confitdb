# ConfitDB

[ConfitDB](https://gitlab.com/liberecofr/confitdb) is an experimental, distributed, real-time database, giving full control on conflict resolution,
implemented in [Rust](https://www.rust-lang.org/).

![ConfitDB icon](https://gitlab.com/liberecofr/confitdb/raw/main/media/confitdb-256x256.jpg)

# Status

Not even a prototype, just an idea. Under heavy construction.

[![Build Status](https://gitlab.com/liberecofr/confitdb/badges/main/pipeline.svg)](https://gitlab.com/liberecofr/confitdb/pipelines)

# Usage

```rust
use confitdb::ConfitDB;

// [TODO] implement this ;)
```

# Notes

Used [this review of rust web frameworks](https://kerkour.com/rust-web-framework-2022)
to pick [warp](https://crates.io/crates/warp) which then lead me to 
[hyper](https://crates.io/crates/hyper) and [tokio](https://crates.io/crates/tokio).

# Links

* [crate](https://crates.io/crates/confitdb) on crates.io
* [doc](https://docs.rs/confitdb/) on docs.rs
* [source](https://gitlab.com/liberecofr/confitdb/tree/main) on gitlab.com

# License

ConfitDB and ConfitUL are licensed under the [MIT](https://gitlab.com/liberecofr/confitdb/blob/main/LICENSE) license.
