use crate::error::Error;
use crate::server_options::ServerOptions;
use hyper;
use hyper::service::{make_service_fn, service_fn};
use hyper::{Body, Request, Response, Server};
use std::convert::Infallible;
use std::hash::Hash;
use std::net::SocketAddr;
use std::sync::{Arc, RwLock};
use std::thread;
use std::time;
use tokio::sync::{mpsc, oneshot};

const HTTP_SERVER_RUN_DELAY_MILLIS: u64 = 10;
const HTTP_SERVER_CHANNEL_CAPACITY: usize = 10;

pub(crate) struct HttpServer {
    options: ServerOptions,
    shutdown_receiver: Option<oneshot::Receiver<String>>,
    msg_sender: mpsc::Sender<Vec<u8>>,
    msg_receiver: mpsc::Receiver<Vec<u8>>,
}

async fn handle(
    req: Request<Body>,
    chan: mpsc::Sender<Vec<u8>>,
) -> Result<Response<Body>, hyper::Error> {
    let body_bytes = hyper::body::to_bytes(req.into_body()).await?;
    // [TODO] check sig here
    let response = format!("OK {}", body_bytes.len());
    Ok(Response::new(response.into()))
}

impl HttpServer {
    pub fn new(options: Option<ServerOptions>) -> Self {
        let real_options = options.unwrap_or(ServerOptions::default());
        let (msg_sender, msg_receiver) = mpsc::channel(HTTP_SERVER_CHANNEL_CAPACITY);
        HttpServer {
            options: real_options,
            shutdown_receiver: None,
            msg_sender,
            msg_receiver,
        }
    }

    async fn wait_shutdown(&mut self) {
        let sr = self.shutdown_receiver.take().unwrap();
        let msg = sr.await.unwrap();
        println!("shutdown: {}", msg);
    }

    pub async fn run(
        &mut self,
        shutdown_receiver: oneshot::Receiver<String>,
        ready_sender: oneshot::Sender<Result<(), Error>>,
        done_sender: oneshot::Sender<Result<(), Error>>,
    ) -> Result<(), Error> {
        self.shutdown_receiver = Some(shutdown_receiver);

        let addr = match format!("{}:{}", self.options.listen_addr, self.options.listen_port)
            .parse::<SocketAddr>()
        {
            Ok(v) => v,
            Err(e) => {
                return Err(Error::invalid_data(
                    format!("unable to parse addr: {:?}", e).as_str(),
                ))
            }
        };

        let msg_sender = self.msg_sender.clone();
        let make_svc = make_service_fn(move |_conn| {
            let msg_sender = msg_sender.clone();
            async move {
                Ok::<_, Infallible>(service_fn(move |req: Request<Body>| {
                    handle(req, msg_sender.clone())
                }))
            }
        });
        let server_builder = match Server::try_bind(&addr) {
            Ok(builder) => builder,
            Err(e) => {
                ready_sender
                    .send(Err(Error::network(format!("{}", e).as_str())))
                    .unwrap();
                return Err(Error::network(format!("{}", e).as_str()));
            }
        };

        // Yield some timeslice, a few msec will not really impair performance
        // but it breaks any code that expects the thread/worker to immediately start.
        // This helps bugs surfacing earlier.
        thread::sleep(time::Duration::from_millis(HTTP_SERVER_RUN_DELAY_MILLIS));

        ready_sender.send(Ok({})).unwrap();
        let graceful = server_builder
            .serve(make_svc)
            .with_graceful_shutdown(self.wait_shutdown());

        if let Err(e) = graceful.await {
            done_sender
                .send(Err(Error::network(format!("{}", e).as_str())))
                .unwrap();
            return Err(Error::network(format!("{}", e).as_str()));
        }
        done_sender.send(Ok({})).unwrap();
        Ok({})
    }
}
