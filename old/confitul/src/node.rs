use crate::range::Range;
use ckey::CKey;
use std::fmt::Formatter;

/// Node is an element of a ring.
///
/// In a typical setup, a host has several nodes which act as "virtual"
/// versioned points. Each node is responsible for keys which are lower,
/// or equal, to its key value, and points to its "next" node.
#[derive(Debug, PartialEq, Eq)]
pub struct Node {
    pub key: CKey,
    next_key: Option<CKey>,
    prev_key: Option<CKey>,
}

impl Node {
    pub fn new_rand() -> Node {
        Self::new_with_key(CKey::rand())
    }

    pub fn new_test(v: f64) -> Node {
        Self::new_with_key(CKey::from(v))
    }

    fn new_with_key(key: CKey) -> Node {
        Node {
            key,
            next_key: None,
            prev_key: None,
        }
    }

    pub fn range(&self) -> Range {
        Range::new(self.prev_key.unwrap_or(self.key), self.key)
    }
}

impl std::fmt::Display for Node {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{{\"key\":{}}}", self.key)
    }
}
