# ConfitUL

[ConfitUL](https://gitlab.com/liberecofr/confitdb/tree/master/confitul) ConfitUL contains utilities for ConfitDB which is an experimental, distributed, real-time database, giving full control on conflict resolution,
implemented in [Rust](https://www.rust-lang.org/).

![ConfitUL icon](https://gitlab.com/liberecofr/confitdb/raw/master/media/confitul-256x256.jpg)

# Status

Not even a prototype, just an idea. Under heavy construction.

# Usage

```rust
use confitul::ConfitUL;

// [TODO] implement this ;)
```

# Links

* [crate](https://crates.io/crates/confitul) on crates.io
* [doc](https://docs.rs/confitul/) on docs.rs
* [source](https://gitlab.com/ufoot/confitul/tree/master) on gitlab.com

# License

ConfitUL is licensed under the [MIT](https://gitlab.com/ufoot/confitul/blob/master/LICENSE) license.
