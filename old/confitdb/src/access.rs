use crate::access_error::AccessError;
use crate::cloud::Cloud;
use crate::conflict::Conflict;
use crate::key_value::KeyValue;
use confitul;
use confitul::CKey;
use confitul::SizedQueueDrain;
use confitul::VClock;
use log::error;
use serde::de::DeserializeOwned;
use serde::Serialize;
use std::cell::RefCell;
use std::marker::PhantomData;

pub struct Access<'a, T, V>
where
    T: Serialize + DeserializeOwned,
    V: Serialize + DeserializeOwned,
{
    pub(crate) author: u64,
    pub(crate) cloud: &'a Cloud<T, V>,
    pub(crate) conflicts: RefCell<SizedQueueDrain<confitul::Conflict<u64, KeyValue>>>,
    pub(crate) phantom: PhantomData<&'a V>,
}

impl<'a, T, V> Access<'a, T, V>
where
    T: Serialize + DeserializeOwned,
    V: Serialize + DeserializeOwned,
{
    pub fn set(&self, key: &str, value: &V) {
        match self.set_raw(key, value) {
            Ok(_) => (),
            Err(e) => {
                error!("can not set key \"{}\": {}", key, e);
            }
        }
    }

    pub fn get(&self, key: &str) -> Option<V> {
        match self.get_raw(key) {
            Ok(v) => v,
            Err(e) => {
                error!("can not get key \"{}\": {}", key, e);
                None
            }
        }
    }

    pub fn remove(&self, key: &str) {
        match self.remove_raw(key) {
            Ok(_) => (),
            Err(e) => {
                error!("can not remove key \"{}\": {}", key, e);
            }
        }
    }

    pub fn resolve(&self, conflict: &Conflict<V>, resolved: Option<&V>) {
        match self.resolve_raw(conflict, resolved) {
            Ok(_) => (),
            Err(e) => {
                error!("can not resolve: {}", e);
            }
        }
    }

    pub fn set_raw(&self, key: &str, value: &V) -> Result<(), AccessError> {
        let l = self.conflicts.borrow().len();
        if l > 0 {
            return Err(AccessError::StillConflicts(l));
        }
        let ck = CKey::digest(&key);
        let serialized = KeyValue::serialize_value(&Some(value))?;
        self.cloud.store_squash_value(
            ck,
            VClock::default(),
            KeyValue {
                key: key.to_string(),
                value: serialized,
            },
            self.author,
        );
        Ok({})
    }

    pub fn get_raw(&self, key: &str) -> Result<Option<V>, AccessError> {
        let ck = CKey::digest(&key);

        match self.cloud.store_get_value(&ck) {
            Some(kv) => KeyValue::deserialize_value(&kv.value),
            None => Ok(None),
        }
    }

    pub fn remove_raw(&self, key: &str) -> Result<(), AccessError> {
        let l = self.conflicts.borrow().len();
        if l > 0 {
            return Err(AccessError::StillConflicts(l));
        }
        let ck = CKey::digest(&key);
        let serialized = KeyValue::serialize_value(&Option::<V>::None)?;
        self.cloud.store_squash_value(
            ck,
            VClock::default(),
            KeyValue {
                key: key.to_string(),
                value: serialized,
            },
            self.author,
        );
        Ok({})
    }

    pub fn resolve_raw(
        &self,
        conflict: &Conflict<V>,
        resolved: Option<&V>,
    ) -> Result<(), AccessError> {
        let l = self.conflicts.borrow().len();
        if l > 0 {
            return Err(AccessError::StillConflicts(l));
        }
        let ck = CKey::digest(&conflict.key);
        let serialized = KeyValue::serialize_value(&resolved)?;
        self.cloud.store_squash_value(
            ck,
            conflict.upstream_version.clone(),
            KeyValue {
                key: conflict.key.clone(),
                value: serialized,
            },
            self.author,
        );
        Ok(())
    }

    pub fn next_raw(&self) -> Option<Result<Conflict<V>, AccessError>> {
        match &self.conflicts.borrow_mut().next() {
            Some(upstream) => Some(Conflict::try_new(upstream)),
            None => None,
        }
    }
}

impl<'a, T, V> Iterator for &Access<'a, T, V>
where
    T: Serialize + DeserializeOwned,
    V: Serialize + DeserializeOwned,
{
    type Item = Conflict<V>;

    fn next(&mut self) -> Option<Conflict<V>> {
        loop {
            let next_raw = self.next_raw();
            match next_raw {
                Some(v) => match v {
                    Ok(conflict) => return Some(conflict),
                    Err(e) => {
                        error!("unable to create conflict, upstream error: {}", e);
                    }
                },
                None => return None,
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::key_value::KeyValue;
    use crate::CKey;
    use crate::Cloud;
    use confitul::VClock;

    #[test]
    fn test_conflict_resolve() {
        let cloud: Cloud<usize, String> = Cloud::new(None).unwrap();
        let access = cloud.access(42);
        let ver1 = VClock::new(1);
        let ver2 = VClock::new(2);
        let key1 = "key1".to_string();
        let key2 = "key2".to_string();
        let val1 = "value1".to_string();
        let val2 = "value2".to_string();
        cloud.store_suggest_value(
            CKey::digest(&key1),
            ver1,
            KeyValue {
                key: key1,
                value: KeyValue::serialize_value(&Some(val1)).unwrap(),
            },
        );
        cloud.store_suggest_value(
            CKey::digest(&key2),
            ver2,
            KeyValue {
                key: key2,
                value: KeyValue::serialize_value(&Some(val2)).unwrap(),
            },
        );
        for conflict in &access {
            let resolved = &conflict.left;
            access.resolve(&conflict, resolved.as_ref());
        }
    }
}
