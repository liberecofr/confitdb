# ConfitDB

[ConfitDB](https://gitlab.com/liberecofr/confitdb/tree/master/confitdb) is an experimental, distributed, real-time database, giving full control on conflict resolution,
implemented in [Rust](https://www.rust-lang.org/).

![ConfitDB icon](https://gitlab.com/liberecofr/confitdb/raw/master/media/confitdb-256x256.jpg)

# Status

Not even a prototype, just an idea. Under heavy construction.

# Usage

```rust
use confitdb::ConfitDB;

// [TODO] implement this ;)
```

# Links

* [crate](https://crates.io/crates/confitdb) on crates.io
* [doc](https://docs.rs/confitdb/) on docs.rs
* [source](https://gitlab.com/liberecofr/confitdb/tree/master) on gitlab.com

# License

ConfitDB is licensed under the [MIT](https://gitlab.com/liberecofr/confitdb/blob/master/LICENSE) license.
