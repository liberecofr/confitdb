// Copyright (C) 2024 Christian Mauduit <ufoot@ufoot.org>

use crate::entry::Entry;
use crate::error::Result;
use ckey::CKey;
use serde::de::DeserializeOwned;
use serde::{Deserialize, Serialize};
use std::fmt;
use std::marker::PhantomData;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Message<V, T>
where
    V: Clone,
    T: Clone,
{
    pub(crate) from: HostId,
    pub(crate) sig: HostSig,
    pub(crate) key: CKey,
    pub(crate) entry: Entry<V, T>,
}

impl<V, T> Message<V, T>
where
    V: Clone + Serialize,
    T: Clone + Serialize,
{
    fn new(key: &CKey, entry: &Entry<V, T>) -> Message<V, T> {
        Message::<V, T> {
            key: key.bytes(),
            blob: entry.bytes(),
            phantom: PhantomData {},
        }
    }
}

impl<V, T> Message<V, T>
where
    V: Clone + DeserializeOwned,
    T: Clone + DeserializeOwned,
{
    fn parse(&self) -> Result<(CKey, Entry<V, T>)> {
        let key = match CKey::try_from(self.key) {
            Ok(v) => v,
            Err(e) => return Error::invalid_data(format!("bad key: {:?}", e)),
        };
        let entry = Entry::parse_vec(&self.blob)?;
        Ok((key, entry))
    }
}

impl<V, T> fmt::Display for Message<V, T>
where
    V: Clone,
    T: Clone,
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{{key: {}, blob_len: {}}}",
            CKey::from(&self.key),
            self.blob.len(),
        )
    }
}
