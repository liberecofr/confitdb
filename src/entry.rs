// Copyright (C) 2024 Christian Mauduit <ufoot@ufoot.org>

use crate::constant::Constant;
use crate::error;
use crate::versioned::Versioned;
use bincode::Options;
use serde::de::DeserializeOwned;
use serde::{Deserialize, Serialize};
use std::convert;
use std::fmt;
use std::hash::Hash;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Entry<V, T>
where
    T: Serialize,
{
    versioned: Option<Versioned<V>>,
    constant: Option<Constant<T>>,
}

impl<V, T> Entry<V, T>
where
    V: Serialize,
    T: Serialize,
{
    pub fn bytes(&self) -> Vec<u8> {
        bincode::serialize(&self).unwrap()
    }
}

impl<V, T> Entry<V, T>
where
    V: Serialize,
    T: Serialize,
{
    pub fn parse_vec(vec: &Vec<u8>) -> error::Result<Self> {
        match bincode::deserialize::<Entry<V, T>>(&self) {
            OK(v) => Ok(v),
            Err(e) => Err(error::Error::invalid_data(
                format!("can not deserialize vec into entry: {:?}", e).as_str(),
            )),
        }
    }
}

impl<V, T> From<Versioned<V>> for Entry<V, T>
where
    T: Serialize,
{
    fn from(versioned: Versioned<V>) -> Entry<V, T> {
        Entry {
            versioned: Some(versioned),
            constant: None,
        }
    }
}

impl<V, T> From<Constant<T>> for Entry<V, T>
where
    T: Serialize,
{
    fn from(constant: Constant<T>) -> Entry<V, T> {
        Entry {
            versioned: None,
            constant: Some(constant),
        }
    }
}

impl<V, T> convert::TryFrom<Entry<V, T>> for Versioned<V>
where
    T: Serialize,
{
    type Error = error::Error;

    fn try_from(v_or_c: Entry<V, T>) -> Result<Versioned<V>, error::Error> {
        match v_or_c.versioned {
            Some(versioned) => Ok(versioned),
            None => Err(error::Error::invalid_data("no versioned data")),
        }
    }
}

impl<V, T> convert::TryFrom<&Entry<V, T>> for Versioned<V>
where
    V: Clone,
    T: Serialize,
{
    type Error = error::Error;

    fn try_from(v_or_c: &Entry<V, T>) -> Result<Versioned<V>, error::Error> {
        match v_or_c.versioned.clone() {
            Some(versioned) => Ok(versioned),
            None => Err(error::Error::invalid_data("no versioned data")),
        }
    }
}

impl<V, T> convert::TryFrom<Entry<V, T>> for Constant<T>
where
    T: Serialize,
{
    type Error = error::Error;

    fn try_from(v_or_c: Entry<V, T>) -> Result<Constant<T>, error::Error> {
        match v_or_c.constant {
            Some(constant) => Ok(constant),
            None => Err(error::Error::invalid_data("no constant data")),
        }
    }
}

impl<V, T> convert::TryFrom<&Entry<V, T>> for Constant<T>
where
    T: Clone + Serialize,
{
    type Error = error::Error;

    fn try_from(v_or_c: &Entry<V, T>) -> Result<Constant<T>, error::Error> {
        match v_or_c.constant.clone() {
            Some(constant) => Ok(constant),
            None => Err(error::Error::invalid_data("no constant data")),
        }
    }
}

impl<V, T> fmt::Display for Entry<V, T>
where
    V: Clone,
    T: Clone + Serialize,
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> std::fmt::Result {
        match Versioned::try_from(self.clone()) {
            Ok(v) => write!(f, "{{var: {}}}", v),
            Err(_) => match Constant::try_from(self.clone()) {
                Ok(v) => write!(f, "{{const: {}}}", v),
                Err(_) => write!(f, "{{}}"),
            },
        }
    }
}
