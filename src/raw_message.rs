// Copyright (C) 2024 Christian Mauduit <ufoot@ufoot.org>

use crate::entry::Entry;
use crate::error::Result;
use ckey::CKey;
use serde::de::DeserializeOwned;
use serde::{Deserialize, Serialize};
use std::fmt;
use std::marker::PhantomData;

#[derive(Debug, Clone, Default)]
pub struct RawMessage {
    blob: Vec<u8>,
}

impl<V, T> Message<V, T>
where
    V: Clone + Serialize,
    T: Clone + Serialize,
{
    fn new() -> Message {
        return Self::default();
    }
}

impl<V, T> fmt::Display for Message<V, T>
where
    V: Clone,
    T: Clone,
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{len: {}}}", self.blob.len(),)
    }
}
