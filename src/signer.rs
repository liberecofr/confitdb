// Copyright (C) 2024 Christian Mauduit <ufoot@ufoot.org>

use crate::host_sig::HostSig;

/// Signer trait used to sign messages.
pub trait Signer {
    fn sign_msg(&self, msg: &[u8]) -> HostSig;
}
