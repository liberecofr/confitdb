// Copyright (C) 2024 Christian Mauduit <ufoot@ufoot.org>

use ckey::CKey;
use serde::de::DeserializeOwned;
use serde::{Deserialize, Serialize};
use std::fmt;

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
pub struct Constant<T>
where
    T: Serialize,
{
    pub value: T,
}

impl<T> Constant<T>
where
    T: Serialize,
{
    pub fn new(value: T) -> Self {
        Constant { value }
    }
}

impl<T> fmt::Display for Constant<T>
where
    T: Serialize,
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> std::fmt::Result {
        let key = CKey::serial(&self.value);
        write!(f, "{{digest: {}}}", key)
    }
}
