// Copyright (C) 2024 Christian Mauduit <ufoot@ufoot.org>

use crate::error::{Error, Result};
use log::warn;
use serde::{Deserialize, Serialize};
use std::fs::{read_to_string, write};
use std::io::Write;

pub const DEFAULT_STORE_DB_PATH: &str = "";
pub const DEFAULT_STORE_CAPACITY: usize = 100_000;
pub const DEFAULT_CACHE_VAR_CAPACITY: usize = 10_000;
pub const DEFAULT_CACHE_CONST_CAPACITY: usize = 10_000;
pub const DEFAULT_LOCAL_HOST_NAME: &str = "host";
pub const DEFAULT_LOCAL_HOST_DESCRIPTION: &str = "service running on host";
pub const DEFAULT_LOCAL_HOST_URL: &str = "http://127.0.0.1";
pub const DEFAULT_MSG_IN_CAPACITY: usize = 1_000;
pub const DEFAULT_MSG_OUT_CAPACITY: usize = 1_000;

#[derive(Debug, Default, Clone, Serialize, Deserialize, Eq, PartialEq)]
pub struct Config {
    pub store_db_path: Option<String>,
    pub store_capacity: Option<usize>,
    pub cache_var_capacity: Option<usize>,
    pub cache_const_capacity: Option<usize>,
    pub local_host_name: Option<String>,
    pub local_host_description: Option<String>,
    pub local_host_urls: Option<Vec<String>>,
    pub msg_in_capacity: Option<usize>,
    pub msg_out_capacity: Option<usize>,
}

impl Config {
    pub fn new() -> Config {
        Self::default()
    }

    pub fn try_from_json(js: &str) -> Result<Config> {
        let cfg: Config = match serde_json::from_str(js) {
            Ok(c) => c,
            Err(e) => return Err(Error::invalid_data(format!("{:?}", e).as_str())),
        };
        Ok(cfg)
    }

    pub fn try_from_yaml(yml: &str) -> Result<Config> {
        let cfg: Config = match serde_yaml::from_str(yml) {
            Ok(c) => c,
            Err(e) => return Err(Error::invalid_data(format!("{:?}", e).as_str())),
        };
        Ok(cfg)
    }

    pub fn to_json(&self) -> String {
        serde_json::json!(self).to_string()
    }

    pub fn to_yaml(&self) -> String {
        serde_yaml::to_string(self).expect("YAML serialization failed")
    }

    pub fn get_store_db_path(&self) -> String {
        self.store_db_path
            .clone()
            .unwrap_or(DEFAULT_STORE_DB_PATH.to_string())
    }

    pub fn get_store_capacity(&self) -> usize {
        self.store_capacity.unwrap_or(DEFAULT_STORE_CAPACITY)
    }

    pub fn get_cache_var_capacity(&self) -> usize {
        self.cache_var_capacity
            .unwrap_or(DEFAULT_CACHE_VAR_CAPACITY)
    }

    pub fn get_cache_const_capacity(&self) -> usize {
        self.cache_const_capacity
            .unwrap_or(DEFAULT_CACHE_CONST_CAPACITY)
    }

    pub fn get_local_host_name(&self) -> String {
        self.local_host_name
            .clone()
            .unwrap_or(DEFAULT_LOCAL_HOST_NAME.to_string())
    }

    pub fn get_local_host_description(&self) -> String {
        self.local_host_description
            .clone()
            .unwrap_or(DEFAULT_LOCAL_HOST_DESCRIPTION.to_string())
    }

    pub fn get_local_host_urls(&self) -> Vec<String> {
        self.local_host_urls
            .clone()
            .unwrap_or(vec![DEFAULT_LOCAL_HOST_URL.to_string()])
    }

    pub fn get_msg_in_capacity(&self) -> usize {
        self.msg_in_capacity.unwrap_or(DEFAULT_MSG_IN_CAPACITY)
    }

    pub fn get_msg_out_capacity(&self) -> usize {
        self.msg_out_capacity.unwrap_or(DEFAULT_MSG_OUT_CAPACITY)
    }

    pub fn read(filename: &str) -> Result<Config> {
        let content = match read_to_string(filename) {
            Ok(c) => c,
            Err(e) => {
                warn!(
                    "could not read config file: '{}', upstream error: {:?}",
                    filename, e
                );
                return Ok(Self::default());
            }
        };
        let lc: String = filename.to_lowercase();
        if lc.ends_with(".yaml") || lc.ends_with(".yml") {
            return Self::try_from_yaml(content.as_str());
        }
        if lc.ends_with(".json") || lc.ends_with(".js") {
            return Self::try_from_json(content.as_str());
        }
        Err(Error::invalid_data(format!("can not guess whether file: {} is JSON or YAML from its extension, please use a file that ends with .json or .yaml",filename).as_str()))
    }

    pub fn write(&self, filename: &str) -> Result<()> {
        let lc: String = filename.to_lowercase();
        let content = if lc.ends_with(".yaml") || lc.ends_with(".yml") {
            serde_yaml::to_string(&self).expect("YAML serialization failed")
        } else if lc.ends_with(".json") || lc.ends_with(".js") {
            serde_json::json!(&self).to_string()
        } else {
            return Err(Error::invalid_data(format!("can not guess whether file: {} is JSON or YAML from its extension, please use a file that ends with .json or .yaml",filename).as_str()));
        };
        match write(filename, content.as_bytes()) {
            Ok(_) => Ok(()),
            Err(e) => Err(Error::storage(
                format!("unable to save config file: {}, {:?}", filename, e).as_str(),
            )),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::fs::File;
    use tempfile::tempdir;

    #[test]
    fn test_read_json() {
        let tmp_dir = tempdir().unwrap();
        let file_path = tmp_dir.path().join("config.json");
        let mut config_file = File::create(&file_path).expect("creation failed");

        // Write contents to the file
        config_file
            .write("{\"store_db_path\": \"/tmp/foo\", \"cache_var_capacity\": 99999}".as_bytes())
            .unwrap();
        config_file.sync_all().unwrap();

        let file_path_str = file_path.as_os_str().to_str().unwrap();
        let cfg = Config::read(file_path_str).expect("reading JSON config failed");

        assert_eq!(Some(String::from("/tmp/foo")), cfg.store_db_path);
        assert_eq!(99_999, cfg.get_cache_var_capacity());
        assert_eq!(10_000, cfg.get_cache_const_capacity());

        drop(config_file);
    }

    #[test]
    fn test_read_yaml() {
        let tmp_dir = tempdir().unwrap();
        let file_path = tmp_dir.path().join("config.yaml");
        let mut config_file = File::create(&file_path).expect("creation failed");

        // Write contents to the file
        config_file
            .write("store_db_path: /tmp/foo\ncache_var_capacity: 99999\n".as_bytes())
            .unwrap();
        config_file.sync_all().unwrap();

        let file_path_str = file_path.as_os_str().to_str().unwrap();
        let cfg = Config::read(file_path_str).expect("reading YAML config failed");

        assert_eq!(Some(String::from("/tmp/foo")), cfg.store_db_path);
        assert_eq!(99_999, cfg.get_cache_var_capacity());
        assert_eq!(10_000, cfg.get_cache_const_capacity());

        drop(config_file);
    }

    #[test]
    fn test_write_json() {
        let mut cfg = Config::new();
        cfg.store_db_path = Some("/tmp/bar".to_string());
        cfg.cache_var_capacity = Some(99_999);

        let tmp_dir = tempdir().unwrap();
        let file_path = tmp_dir.path().join("config.json");
        let file_path_str = file_path.as_os_str().to_str().unwrap();

        cfg.write(file_path_str)
            .expect("could not write JSON config");

        let content = read_to_string(file_path_str).expect("could not read JSON content");
        assert!(content.contains("\"store_db_path\":\"/tmp/bar\""));

        let cfg_check = Config::read(file_path_str).expect("reading JSON config failed");

        assert_eq!(cfg_check, cfg);
    }

    #[test]
    fn test_write_yaml() {
        let mut cfg = Config::new();
        cfg.store_db_path = Some("/tmp/bar".to_string());
        cfg.cache_var_capacity = Some(99_999);

        let tmp_dir = tempdir().unwrap();
        let file_path = tmp_dir.path().join("config.yaml");
        let file_path_str = file_path.as_os_str().to_str().unwrap();

        cfg.write(file_path_str)
            .expect("could not write YAML config");

        let content = read_to_string(file_path_str).expect("could not read YAML content");
        assert!(content.contains("store_db_path: /tmp/bar"));

        let cfg_check = Config::read(file_path_str).expect("reading YAML config failed");

        assert_eq!(cfg_check, cfg);
    }
}
