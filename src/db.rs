// Copyright (C) 2024 Christian Mauduit <ufoot@ufoot.org>

use crate::config::*;
use crate::constant::Constant;
use crate::entry::Entry;
use crate::error::{Error, Result};
use crate::host::Host;
use crate::host_id::HostId;
use crate::host_info::HostInfo;
use crate::local_host::LocalHost;
use crate::message::Message;
use crate::versioned::Versioned;
use ckey::CKey;
use hashlru::SyncCache;
use log::{info, trace};
use menhirkv::Store;
use serde::de::DeserializeOwned;
use serde::Serialize;
use std::thread;
use tokio::runtime::Builder;
use tokio::sync::{mpsc, oneshot};
use vclock::VClock64;

const STORE_CF_VAR: &str = "var";
const STORE_CF_CONST: &str = "const";

/// Database object, used to store and retrive values, handling conflicts.
///
/// How does it differ from a regular key-value store? It has 2 different domains.
///
/// Variables ("var" series of functions) can be updated, whenever a conflict is
/// detected, that is, when 2 different authors/hosts try to modify a value without
/// getting an agreement, then the 2 values are stored. This is why "get()" on a variable
/// value returns a vector of value. In the simple case, there's only one value, or none,
/// if there's no value for it or if the value exists and is non-controversial.
///
/// When several possibilities exist -> a list of all of them is returned. It is the
/// responsibility of the caller to sort that out, resolve the conflict, and store
/// everything back. Resolving the conflict means reading the values from the vector,
/// taking (possibly arbitrary...) decisions on what version is the best, then
/// just use a simple put to resolve the conflict.
///
/// Constants ("const" series of functions) can not be updated. You do not give a key,
/// instead you store the value, and the key for it is given back to you.
///
/// This key can then be used to retrieve the value, it is based on a hash of the stored
/// value so it's not possible to alter the value... it would change the key. Collisions
/// are technically possible but with 256-bit keys they would likely be the consequences
/// of an explicit cryptographic attack rather than just "bad luck".
///
/// # Examples
/// ```
/// use confitdb::DB;
///
/// let db: DB<usize, usize> = DB::new(None).unwrap(); // using defaults
///
/// // Feels like a KV-store but array of values returned, and not single-value.
/// db.put_var("test", 123).unwrap();
/// assert_eq!(Ok(vec![Some(123)]), db.get_var("test"));
/// // It is possible to update.
/// db.put_var("test", 789).unwrap();
/// assert_eq!(Ok(vec![Some(789)]), db.get_var("test"));
/// // Delete puts none as a value, but does not remove entry.
/// db.delete_var("test").unwrap();
/// assert_eq!(Ok(vec![None]), db.get_var("test"));
///
/// // Now dealing with constants.
/// let key = db.put_const(456).unwrap();
/// // key is picked up from value, consider it as a hash
/// assert_eq!("33c71a969c8e94d14503e491b1b8a83188df85620cd7a3b0fe35f3edee245790", key.as_str());
/// // key can be used to retrive value later
/// let value = db.get_const(key.as_str());
/// assert_eq!(Ok(Some(456)), db.get_const(key.as_str()));
/// ```
pub struct DB<V, T>
where
    V: Eq + Clone + Serialize + DeserializeOwned + Sync + Send,
    T: Clone + Serialize + DeserializeOwned + Sync + Send,
{
    config: Config,
    local_host: LocalHost,
    cache_var: SyncCache<CKey, Vec<Versioned<V>>>,
    cache_const: SyncCache<CKey, Constant<T>>,
    store: Store<CKey, Entry<V, T>>,
    msg_out_sender: Option<mpsc::Sender<Message<V, T>>>,
    msg_in_receiver: Option<mpsc::Receiver<Message<V, T>>>,
}

impl<V, T> DB<V, T>
where
    V: Eq + Clone + Serialize + DeserializeOwned + Sync + Send,
    T: Clone + Serialize + DeserializeOwned + Sync + Send,
{
    /// Crate a new database.
    ///
    /// # Examples
    /// ```
    /// use confitdb::{Config, DB};
    ///
    /// let mut config = Config::new();
    /// config.store_capacity = Some(1_000_000);
    /// let db: DB<usize, usize> = DB::new(Some(config)).unwrap();
    /// print!("{}", db.local_host_info());
    /// ```
    pub fn new(config: Option<Config>) -> Result<DB<V, T>> {
        let real_config = config.unwrap_or(Config::default());
        let store_db_path = real_config.get_store_db_path();
        let store_result = if store_db_path.len() > 0 {
            Store::open_cf_with_path(
                &store_db_path,
                &[STORE_CF_VAR, STORE_CF_CONST],
                real_config.get_store_capacity(),
            )
        } else {
            Store::open_cf_temporary(
                &[STORE_CF_VAR, STORE_CF_CONST],
                real_config.get_store_capacity(),
            )
        };
        let store = match store_result {
            Ok(store) => store,
            Err(e) => {
                return Err(Error::storage(
                    format!(
                        "unable to create DB store, path=\"{}\", upstream error={:?}",
                        store_db_path, e
                    )
                    .as_str(),
                ))
            }
        };
        let local_host = match LocalHost::new(Some(real_config.clone())) {
            Ok(h) => h,
            Err(e) => {
                return Err(Error::invalid_data(
                    format!("unable to create local host, upstream error={:?}", e).as_str(),
                ))
            }
        };
        let cache_var = SyncCache::new(real_config.get_cache_var_capacity());
        let cache_const = SyncCache::new(real_config.get_cache_const_capacity());
        Ok(DB {
            config: real_config,
            local_host,
            cache_var,
            cache_const,
            store,
            msg_out_sender: None,
            msg_in_receiver: None,
        })
    }

    async fn pump_local_only(mut msg_out_receiver: mpsc::Receiver<Message<V, T>>) {
        while let Some(msg) = msg_out_receiver.recv().await {
            trace!("got outbound message, dropping it: {}", msg);
        }
        msg_out_receiver.close();
        info!("stopping local pump");
    }

    fn start_local_only(&mut self) -> Result<()> {
        let (msg_out_sender, msg_out_receiver) = mpsc::channel(self.config.get_msg_out_capacity());
        self.msg_out_sender = Some(msg_out_sender);
        thread::spawn(move || {
            Builder::new_multi_thread()
                .enable_all()
                .build()
                .unwrap()
                .block_on(Self::pump_local_only(msg_out_receiver))
        });
        Ok(())
    }

    fn start(&mut self) -> Result<()> {
        self.stop()?; // should be idempotent

        // [TODO] if using a real web-server -> use the in channel, which would
        // just get filled with data from incoming requests.
        self.start_local_only()
    }

    fn stop(&mut self) -> Result<()> {
        match self.msg_out_sender.take() {
            Some(msg_out_sender) => {
                info!("dropping msg out sender, this should stop consumer");
                drop(msg_out_sender);
            }
            None => info!("msg out sender already dropped"),
        }
        match self.msg_in_receiver.take() {
            Some(mut msg_in_receiver) => {
                msg_in_receiver.close();
                while let Ok(msg) = msg_in_receiver.try_recv() {
                    trace!("leftover inbound message, dropping it: {}", msg);
                }
                drop(msg_in_receiver);
            }
            None => info!("msg in receiver already dropped"),
        }
        Ok(())
    }

    fn store_var(&self) -> Store<CKey, Entry<V, T>> {
        self.store
            .cf(STORE_CF_VAR)
            .expect(format!("unable to open column family {}", STORE_CF_VAR).as_str())
    }

    fn store_const(&self) -> Store<CKey, Entry<V, T>> {
        self.store
            .cf(STORE_CF_CONST)
            .expect(format!("unable to open column family {}", STORE_CF_CONST).as_str())
    }

    pub fn local_host_info(&self) -> HostInfo {
        self.local_host.info().clone()
    }

    pub fn get_var(&self, key: &str) -> Result<Vec<Option<V>>> {
        let k = CKey::digest(key);
        match self.cache_var.get(&k) {
            Some(v) => {
                // Got it in mem cache, serve it as is, minus the version information
                Ok(v.into_iter().map(|x| x.value).collect())
            }
            None => match self.store_var().get(&k) {
                Ok(v) => match v {
                    Some(v) => {
                        // Got it in persistent store, try to convert it.
                        let v = match Versioned::try_from(v) {
                            Ok(v) => v,
                            Err(e) => {
                                return Err(Error::invalid_data(
                                    format!(
                                        "unable to read versioned data from var {} ({}): {:?}",
                                        key, k, e
                                    )
                                    .as_str(),
                                ))
                            }
                        };
                        // Sync the mem cache.
                        self.cache_var.push(k, vec![v.clone()]);
                        // Return the value, minus the version information.
                        Ok(vec![v.value])
                    }
                    // There's really nothing, this is unknown.
                    None => Ok(vec![]),
                },
                Err(e) => Err(Error::storage(
                    format!("could not get store var {} ({}): {:?}", key, k, e).as_str(),
                )),
            },
        }
    }

    fn put_or_delete_var(&self, key: &str, value: Option<V>) -> Result<()> {
        let k = CKey::digest(key);
        let version: VClock64<HostId> = VClock64::new(self.local_host.id());
        let versioned: Versioned<V> = Versioned::new(version, value);
        let entry = Entry::from(versioned.clone());

        match self.store_var().put(&k, &entry) {
            Ok(_) => (),
            Err(e) => {
                return Err(Error::storage(
                    format!("could not put store var {} ({}): {:?}", key, k, e).as_str(),
                ))
            }
        };

        self.cache_var.push(k, vec![versioned]);

        Ok(())
    }

    pub fn put_var(&self, key: &str, value: V) -> Result<()> {
        self.put_or_delete_var(key, Some(value))
    }

    pub fn delete_var(&self, key: &str) -> Result<()> {
        self.put_or_delete_var(key, None)
    }

    pub async fn async_put_var(&self, key: &str, value: V) -> Result<()> {
        Ok(())
    }

    pub async fn async_delete_var(&self, key: &str, value: V) -> Result<()> {
        Ok(())
    }

    pub fn get_const(&self, key: &str) -> Result<Option<T>> {
        let k = match CKey::parse(key) {
            Ok(k) => k,
            Err(e) => {
                return Err(Error::invalid_data(
                    format!("invalid key for get const ({}): {:?}", key, e).as_str(),
                ))
            }
        };
        match self.cache_const.get(&k) {
            Some(v) => {
                // Got it in mem cache, serve it as is, minus the constant enveloppe
                Ok(Some(v.value))
            }
            None => match self.store_const().get(&k) {
                Ok(v) => match v {
                    Some(v) => {
                        // Got it in persistent store, try to convert it.
                        let v = match Constant::try_from(v) {
                            Ok(v) => v,
                            Err(e) => {
                                return Err(Error::invalid_data(
                                    format!(
                                        "unable to read versioned data from const {} ({}): {:?}",
                                        key, k, e
                                    )
                                    .as_str(),
                                ))
                            }
                        };
                        // Sync the mem cache.
                        self.cache_const.push(k, v.clone());
                        // Return the value, minus the constant enveloppe
                        Ok(Some(v.value))
                    }
                    // There's really nothing, this is unknown.
                    None => Ok(None),
                },
                Err(e) => Err(Error::storage(
                    format!("could not get store const {} ({}): {:?}", key, k, e).as_str(),
                )),
            },
        }
    }

    pub fn put_const(&self, value: T) -> Result<String> {
        let k = CKey::serial(&value);
        let constant: Constant<T> = Constant::new(value);
        let entry = Entry::from(constant.clone());

        match self.store_const().put(&k, &entry) {
            Ok(_) => (),
            Err(e) => {
                return Err(Error::storage(
                    format!("could not put store const ({}): {:?}", k, e).as_str(),
                ))
            }
        };

        self.cache_const.push(k, constant);

        Ok(format!("{:?}", k))
    }

    pub fn delete_const(&self, key: &str) -> Result<()> {
        match CKey::parse(key) {
            Ok(k) => match self.store_const().delete(&k) {
                Ok(_) => {
                    self.cache_const.delete(&k);
                    Ok(())
                }
                Err(e) => Err(Error::storage(
                    format!("unable to remove key from store const ({}): {:?}", key, e).as_str(),
                )),
            },
            Err(e) => Err(Error::invalid_data(
                format!("invalid key for delete const ({}): {:?}", key, e).as_str(),
            )),
        }
    }

    pub async fn async_put_const(&self, value: T) -> Result<String> {
        Ok(String::from("[TODO]"))
    }

    pub async fn async_delete_const(&self, key: &str) -> Result<()> {
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_new() {
        let db: DB<u32, u32> = DB::new(None).expect("db creation failed");
        let info = db.local_host_info();
        assert_eq!(11, format!("{}", info.id).len());
        assert_eq!(9, format!("{}", info.sig).len());
    }

    #[test]
    fn test_put_get_var() {
        let db: DB<u32, u32> = DB::new(None).expect("db creation failed");

        db.put_var("test", 123).expect("put failed");
        let check = db.get_var("test").expect("get failed");
        // We get the value we expect, in an array of one item,
        // which means there are no conflicts, the message is clear,
        // there is one and one unique valid value for this key.
        assert_eq!(vec![Some(123)], check);
    }

    #[test]
    fn test_put_get_var_multiple() {
        let db: DB<u32, u32> = DB::new(None).expect("db creation failed");

        db.put_var("test", 123).expect("put failed");
        db.put_var("test", 456).expect("put failed");
        db.put_var("test", 789).expect("put failed");
        let check = db.get_var("test").expect("get failed");
        // Regardless of how many puts there's been, there's only
        // one version left, as we're the single author so there's no
        // possible conflict, we have full authority on everything.
        assert_eq!(vec![Some(789)], check);
    }

    #[test]
    fn test_put_delete_var() {
        let db: DB<u32, u32> = DB::new(None).expect("db creation failed");

        db.put_var("test", 123).expect("put failed");
        db.delete_var("test").expect("delete failed");
        let check = db.get_var("test").expect("get failed");
        // What we get is an array with one empty item.
        // What it means is that there's is one and one unique valid
        // state for this entry, and the current state is: there is no data.
        assert_eq!(vec![None], check);
    }

    #[test]
    fn test_put_get_const() {
        let db: DB<u32, u32> = DB::new(None).expect("db creation failed");

        let key = db.put_const(123).expect("put failed");
        let check = db.get_const(key.as_str()).expect("get failed");
        // We get our constant back.
        assert_eq!(Some(123), check);
        // Nit, checking key size (256 bits, 64 chars in hex. representation)
        assert_eq!(64, key.len());
    }

    #[test]
    fn test_put_get_const_multiple() {
        let db: DB<u32, u32> = DB::new(None).expect("db creation failed");

        let key1 = db.put_const(123).expect("put failed");
        let key2 = db.put_const(123).expect("put failed");
        let key3 = db.put_const(456).expect("put failed");
        let check1 = db.get_const(key1.as_str()).expect("get failed");
        let check2 = db.get_const(key2.as_str()).expect("get failed");
        let check3 = db.get_const(key3.as_str()).expect("get failed");
        // We get our constant back.
        assert_eq!(Some(123), check1);
        assert_eq!(Some(123), check2);
        assert_eq!(Some(456), check3);
        // Additionnally, keys are stable...
        assert_eq!(key1, key2);
    }

    #[test]
    fn test_put_delete_const() {
        let db: DB<u32, u32> = DB::new(None).expect("db creation failed");

        let key = db.put_const(123).expect("put failed");
        db.delete_const(key.as_str()).expect("delete failed");
        let check = db.get_const(key.as_str()).expect("get failed");
        // No traces left.
        assert_eq!(None, check);
    }
}
